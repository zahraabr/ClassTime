package com.classtime.ClassTime.controller;

import com.classtime.ClassTime.event.Schedule;
import com.classtime.ClassTime.service.*;
import com.classtime.ClassTime.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.*;

@Controller
public class ScheduleController{
    private static final String strSchedules = "schedules";
    private static final String strSchedule = "schedule";

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private ScheduleListService scheduleListService;

    @GetMapping("/addschedule")
    public String showScheduleForm(Schedule schedule) {
        return "add-schedule";
    }

    @PostMapping("/addschedule")
    public String addSchedule(@Valid Schedule schedule, BindingResult result, Model model) {
        try{
            if (schedule.getDayOfWeek() == DayOfWeek.SATURDAY || schedule.getDayOfWeek() == DayOfWeek.SUNDAY){
                return "error-date";
            }
            scheduleListService.addSchedule(schedule);
            model.addAttribute(strSchedules, scheduleListService.findAllOrderByDateAsc());
            return "index";
        }catch (Exception e){
            return "add-schedule";
        }

        //        if (result.hasErrors()) {
//            return "add-schedule";
//        }
//
//        scheduleRepository.save(schedule);
//        model.addAttribute("schedules", userRepository.findAll());
//        return "index";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model){
        Schedule schedule = scheduleRepository.findById(id)
            .orElseThrow(() -> new IllegalArgumentException ("Invalid schedule id:" + id));

        model.addAttribute(strSchedule, schedule);
        return "update-schedule";
    }

    @PostMapping("/update/{id}")
    public ModelAndView updateSchedule(@PathVariable("id") long id, @Valid Schedule schedule,
                             BindingResult result, Model model) {
        try{
            if (schedule.getDayOfWeek() == DayOfWeek.SATURDAY || schedule.getDayOfWeek() == DayOfWeek.SUNDAY){
                return new ModelAndView("redirect:/error-date");
            }
            scheduleListService.addSchedule(schedule);
            model.addAttribute(strSchedules, scheduleRepository.findAllOrderByDateAsc());
            return new ModelAndView("redirect:/");
//            return "index";
        }catch (Exception e){
            schedule.setId(id);
            return new ModelAndView("redirect:/update/" + Long.toString(id));
//            return "update-schedule";
        }
        //        if (result.hasErrors()) {
//            schedule.setId(id);
//            return "update-schedule";
//        }
//
//        scheduleRepository.save(schedule);
//        model.addAttribute("courses", userRepository.findAll());
//        return "index";
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deleteSchedule(@PathVariable("id") long id, Model model) {
        Schedule schedule = scheduleRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid schedule id:" + id));
        scheduleRepository.delete(schedule);
        model.addAttribute(strSchedules, scheduleListService.findAllOrderByDateAsc());
        return new ModelAndView("redirect:/");
//        return "index";
    }

//    @GetMapping(value = "/deleteschedule")
//    private String deleteScheduleForm() {
//        return "delete-schedule";
//    }
//
//    @GetMapping("deleteschedule/{id}")
//    public String deleteSchedule(@PathVariable("courseID") long courseID, Model model) {
//        try{
//            Schedule schedule = courseRepository.findById(courseID).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + courseID));
//            scheduleRepository.delete(schedule);
//            model.addAttribute(strSchedule, scheduleRepository.findAll());
//            return "index";
//        }
//        catch (Exception e){
//            return "delete-schedule";
//        }
//    }

    //Schedule List View
    @GetMapping(value = "/")
    public String viewScheduleList(@ModelAttribute(strSchedules) Schedule schedule,Model model) {
        model.addAttribute(strSchedules, scheduleListService.findAllOrderByDateAsc());
        return "index";
    }

    @GetMapping(value = "/error-date")
    public String viewErrorDate() {
        return "error-date";
    }
}

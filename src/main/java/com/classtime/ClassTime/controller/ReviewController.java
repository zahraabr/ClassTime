//package com.classtime.ClassTime.controller;
//
//import com.classtime.ClassTime.event.*;
//import com.classtime.ClassTime.repository.*;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//
//import javax.validation.Valid;
//import java.time.*;
//
//@Controller
//public class ReviewController {
//    private static final String strSchedule = "schedule";
//    private static final String strSchedules = "schedules";
//    private static final String strReviews = "reviews";
//    private static final String strReview = "review";
//
//    @Autowired
//    private ScheduleRepository scheduleRepository;
//
//    @Autowired
//    private ReviewRepository reviewRepository;
//
////    @Autowired
////    private ScheduleListService scheduleListService;
//
//
//    @GetMapping("/addreview/{id}")
//    public String showReviewForm(@PathVariable("id") long id, Model model) {
//        model.addAttribute(strReview, new Review());
//        model.addAttribute(strSchedule, id);
//        return "add-review";
//    }
//
//    @PostMapping("/addreview/{id}")
//    public String addReview(@PathVariable("id") long id, @Valid Review review, BindingResult result, Model model) {
//        try{
//            Schedule schedule = scheduleRepository.findById(id).get();
//            Review newReview = new Review(schedule, review.getDate(), review.getTitle(), review.getContent());
//            reviewRepository.save(review);
//            model.addAttribute(strReviews, reviewRepository.findAllOrderByDateAsc());
//            return "review";
//        }catch (Exception e){
//            return "add-review";
//        }
//    }
//
//    @GetMapping("/editreview/{id}")
//    public String showUpdateForm(@PathVariable("id") long id, Model model){
//        Review review = reviewRepository.findById(id)
//                .orElseThrow(() -> new IllegalArgumentException ("Invalid review id:" + id));
//
//        model.addAttribute(strReview, review);
//        return "update-review";
//    }
//
//    @PostMapping("/updatereview/{id}")
//    public String updateReview(@PathVariable("id") long id, @Valid Review review,
//                                 BindingResult result, Model model) {
//        try{
//            reviewRepository.save(review);
//            model.addAttribute(strSchedules, scheduleRepository.findAllOrderByDateAsc());
//            model.addAttribute(strReviews, reviewRepository.findAllOrderByDateAsc());
//            return "review";
//        }catch (Exception e){
//            review.setId(id);
//            return "update-review";
//        }
//    }
//
//    @GetMapping("/deletereview/{id}")
//    public String deleteReview(@PathVariable("id") long id, Model model) {
//        Review review = reviewRepository.findById(id)
//                .orElseThrow(() -> new IllegalArgumentException("Invalid review id:" + id));
//        reviewRepository.delete(review);
//        model.addAttribute(strReviews, reviewRepository.findAllOrderByDateAsc());
//        return "review";
//    }
//
//    @GetMapping("/seereview/{id}")
//    public String seeReview(@PathVariable("id") long id, Model model) {
//        Review review = reviewRepository.findById(id)
//                .orElseThrow(() -> new IllegalArgumentException("Invalid review id:" + id));
//        Schedule schedule = review.getSchedule();
//        model.addAttribute(strSchedule, schedule);
//        model.addAttribute(strReview, review);
//        return "see-review";
//    }
//
//    //Review List View
//    @GetMapping(value = "/review")
//    private String viewReviewList(@ModelAttribute(strReviews) Review review, @ModelAttribute(strSchedules) Schedule schedule, Model model) {
//        model.addAttribute(strReviews, reviewRepository.findAllOrderByDateAsc());
//        model.addAttribute(strSchedules, scheduleRepository.findAllOrderByDateAsc());
//        return "review";
//    }
//}

package com.classtime.ClassTime.controller;


import com.classtime.ClassTime.event.*;
import com.classtime.ClassTime.repository.*;
import com.classtime.ClassTime.service.*;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.*;

@Controller
public class ReviewController {
    private static final String strSchedule = "schedule";
    private static final String strSchedules = "schedules";
    private static final String strReviews = "reviews";
    private static final String strReview = "review";

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private ScheduleListService scheduleListService;

    @Autowired
    private ReviewListService reviewListService;

    @GetMapping("/addreview/{id}")
    public String showReviewForm(@PathVariable("id") long id, Model model) {
        model.addAttribute(strReview, new Review());
        model.addAttribute(strSchedule, id);
        return "add-review";
    }

    @PostMapping("/addreview/{id}")
    public ModelAndView addReview(@PathVariable("id") long id, @Valid Review review, BindingResult result, Model model) {
        try{
            reviewListService.addReview(review);
            model.addAttribute(strReviews, reviewListService.findAllOrderByDateAsc());
            return new ModelAndView("redirect:/review");
        }catch (Exception e){
            return new ModelAndView("redirect:/addreview/" + Long.toString(id));
        }
    }

    @GetMapping("/editreview/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model){
        Review review = reviewRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException ("Invalid review id:" + id));

        model.addAttribute(strReview, review);
        return "update-review";
    }

    @PostMapping("/updatereview/{id}")
    public ModelAndView updateReview(@PathVariable("id") long id, @Valid Review review,
                                     BindingResult result, Model model) {
        try{
            reviewListService.addReview(review);
            model.addAttribute(strReviews, reviewListService.findAllOrderByDateAsc());
            model.addAttribute(strSchedules, scheduleListService.findAllOrderByDateAsc());
            return new ModelAndView("redirect:/review");
        }catch (Exception e){
            review.setId(id);
            return new ModelAndView("redirect:/updatereview/" + Long.toString(id));
        }
    }

    @GetMapping("/deletereview/{id}")
    public ModelAndView deleteReview(@PathVariable("id") long id, Model model) {
        Review review = reviewRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid review id:" + id));
        reviewRepository.delete(review);
        model.addAttribute(strReviews, reviewListService.findAllOrderByDateAsc());
        return new ModelAndView("redirect:/review");
    }

    @GetMapping("/seereview/{id}")
    public String seeReview(@PathVariable("id") long id, Model model) {
        Review review = reviewRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid review id:" + id));
        model.addAttribute(strSchedule, review.getSchedule());
        model.addAttribute(strReview, review);
        review.setId(id);
//        return new ModelAndView("redirect:/seereview/" + Long.toString(id));
        return "see-review";
    }

    //Review List View
    @GetMapping(value = "/review")
    public String viewReviewList(@ModelAttribute(strReviews) Review review, @ModelAttribute(strSchedules) Schedule schedule, Model model) {
        model.addAttribute(strReviews, reviewListService.findAllOrderByDateAsc());
        model.addAttribute(strSchedules, scheduleListService.findAllOrderByDateAsc());
        return "review";
    }
}
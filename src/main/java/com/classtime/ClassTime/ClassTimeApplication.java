package com.classtime.ClassTime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassTimeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClassTimeApplication.class, args);
	}

}

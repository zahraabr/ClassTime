package com.classtime.ClassTime.repository;

import com.classtime.ClassTime.event.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ReviewRepository extends CrudRepository<Review, Long> {
    @Query("FROM Review ORDER BY date")
    List<Review> findAllOrderByDateAsc();
}
package com.classtime.ClassTime.repository;

import com.classtime.ClassTime.event.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ScheduleRepository extends CrudRepository<Schedule, Long> {
    @Query("FROM Schedule ORDER BY date, startTime, endTime ASC")
    List<Schedule> findAllOrderByDateAsc();
//
//    Schedule findByScheduleId(long id);
}

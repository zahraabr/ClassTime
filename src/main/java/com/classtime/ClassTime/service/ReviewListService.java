package com.classtime.ClassTime.service;

import com.classtime.ClassTime.event.*;
import java.util.List;

public interface ReviewListService {
    List<Review> findAllOrderByDateAsc();

    Review addReview(Review review);
}
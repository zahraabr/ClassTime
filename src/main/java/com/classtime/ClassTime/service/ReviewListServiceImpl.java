package com.classtime.ClassTime.service;

import com.classtime.ClassTime.event.*;
import com.classtime.ClassTime.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ReviewListServiceImpl implements ReviewListService{
    @Autowired
    private ReviewRepository reviewRepository;

    @Override
    public List<Review> findAllOrderByDateAsc() {
        return reviewRepository.findAllOrderByDateAsc();
    }

    @Override
    public Review addReview(Review review) {
        return reviewRepository.save(review);
    }
}
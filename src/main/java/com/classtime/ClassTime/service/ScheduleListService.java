package com.classtime.ClassTime.service;

import com.classtime.ClassTime.event.*;
import java.util.List;

public interface ScheduleListService {
    List<Schedule> findAllOrderByDateAsc();

    Schedule addSchedule(Schedule schedule);
}
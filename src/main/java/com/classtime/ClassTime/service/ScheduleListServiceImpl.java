package com.classtime.ClassTime.service;

import com.classtime.ClassTime.event.*;
import com.classtime.ClassTime.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ScheduleListServiceImpl implements ScheduleListService{
    @Autowired
    private ScheduleRepository scheduleRepository;

    @Override
    public List<Schedule> findAllOrderByDateAsc() {
        return scheduleRepository.findAllOrderByDateAsc();
    }

    @Override
    public Schedule addSchedule(Schedule schedule) {
        return scheduleRepository.save(schedule);
    }
}
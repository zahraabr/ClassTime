package com.classtime.ClassTime.controller;

import com.classtime.ClassTime.event.*;
import com.classtime.ClassTime.repository.*;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import java.time.*;

//@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ReviewControllerTest {
    @Autowired
    private MockMvc mvc;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private Model model;

    @MockBean
    private ScheduleRepository scheduleRepository;
    private Schedule schedule = new Schedule();

    @MockBean
    private ReviewRepository reviewRepository;
    private Review review = new Review();

    @BeforeEach
    void SetUpReview() {
        schedule = new Schedule();
        schedule.setId(123);
        schedule.setName("Software Engineering");
        schedule.setLink("https://meet.google.com/efd-osuj-vcz");
        schedule.setDate(LocalDate.parse("2020-06-03"));
        schedule.setStartTime(LocalTime.parse("09:00"));
        schedule.setStartTime(LocalTime.parse("10:00"));
        when(scheduleRepository.findById(schedule.getId()))
                .thenReturn(java.util.Optional.ofNullable(schedule));
        review = new Review();
        review.setId(0);
        review.setTitle("Software Engineering");
        review.setSchedule(schedule);
        review.setContent("test");
        review.setDate(LocalDate.now());
        when(reviewRepository.findById(review.getId()))
                .thenReturn(java.util.Optional.ofNullable(review));
    }

    @Test
    public void GETaddReview() throws Exception {
        mvc.perform(get("/addreview/" + schedule.getId())).andExpect(status().isOk()).andExpect(view().name("add-review"));
    }

    @Test
    public void viewReviewList() throws Exception {
        mvc.perform(get("/review"))
                .andExpect(status().isOk())
                .andExpect(view().name("review"));
    }

    @Test
    public void checkIfShowReviewForm() throws Exception {
        mvc.perform(get("/addreview/" + review.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("add-review"));
    }

    @Test
    void checkIfAddReviewValidThenShowTemplateIfNotValidTheStay() throws Exception {
        mvc.perform(post("/addreview/" + review.getId())
                .flashAttr("review", review)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/review"));
    }

    @Test
    void checkIfAddReviewNotValidThenStay() throws Exception {
        review.setContent(null);
        mvc.perform(post("/addreview/" + review.getId())
                .flashAttr("review", review)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(view().name("redirect:/review"));
    }

    @Test
    void checkIfValidThenShowUpdateFormTemplate() throws Exception{
        mvc.perform(get("/editreview/" + review.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("update-review"))
                .andExpect(model().attributeExists("review"));
    }

    @Test
    void checkIfShowUpdateFormTemplateButIdNotFoundThenThrowException() {
        try {
            mvc.perform(get("/editreview/0"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("update-review"))
                    .andExpect(model().attributeExists("review"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid review id:0",
                    e.getMessage());
        }
    }

    @Test
    void checkIfUpdateReviewValidThenShowTemplate() throws Exception {
        mvc.perform(post("/updatereview/" + review.getId())
                .flashAttr("review", review)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/review"));
    }

    @Test
    void checkIfUpdateReviewNotValidThenStay() throws Exception {
        review.setContent(null);
        mvc.perform(post("/updatereview/" +  review.getId())
                .flashAttr("review", review)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/review"));
    }

    @Test
    void checkIfValidThenShowDeleteScheduleTemplate() throws Exception{
        mvc.perform(get("/deletereview/" + review.getId()))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/review"));
    }

    @Test
    void checkIfSeeReviewButIdNotFoundThenThrowException() {
        try {
            mvc.perform(get("/seereview/" + review.getId()))
                    .andExpect(status().isOk())
                    .andExpect(view().name("see-review"))
                    .andExpect(model().attributeExists("review"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid review id:0",
                    e.getMessage());
        }
    }

    @Test
    void checkIfShowDeleteReviewTemplateButIdNotFoundThenThrowException() {
        try {
            mvc.perform(get("/deletereview/0"))
                    .andExpect(status().isFound())
                    .andExpect(view().name("redirect:/review"));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid review id:0",
                    e.getMessage());}
    }
}

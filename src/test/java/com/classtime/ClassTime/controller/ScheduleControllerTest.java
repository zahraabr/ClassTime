package com.classtime.ClassTime.controller;

import com.classtime.ClassTime.event.Schedule;
import com.classtime.ClassTime.repository.ScheduleRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import java.time.*;

//@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ScheduleControllerTest {
    @Autowired
    private MockMvc mvc;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private Model model;

    @MockBean
    private ScheduleRepository scheduleRepository;
    private Schedule schedule = new Schedule();

    @BeforeEach
    void SetUpSchedule() {
        schedule = new Schedule();
        schedule.setId(123);
        schedule.setName("Software Engineering");
        schedule.setLink("https://meet.google.com/efd-osuj-vcz");
        schedule.setDate(LocalDate.parse("2020-06-03"));
        schedule.setStartTime(LocalTime.parse("09:00"));
        schedule.setStartTime(LocalTime.parse("10:00"));
        when(scheduleRepository.findById(schedule.getId()))
                .thenReturn(java.util.Optional.ofNullable(schedule));
    }

    @Test
    public void addScheduleGET() throws Exception {
        mvc.perform(get("/addschedule")).andExpect(status().isOk()).andExpect(view().name("add-schedule"));
    }

    @Test
    public void errorDateGET() throws Exception {
        mvc.perform(get("/error-date")).andExpect(status().isOk()).andExpect(view().name("error-date"));
    }

    @Test
    public void viewScheduleList() throws Exception {
        mvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void checkIfShowScheduleForm() throws Exception {
        mvc.perform(get("/addschedule"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-schedule"));
    }

    @Test
    void checkIfAddScheduleValidThenShowTemplateIfNotValidTheStay() throws Exception {
        mvc.perform(post("/addschedule")
                .flashAttr("schedule", schedule)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    void checkIfAddScheduleNotValidThenStay() throws Exception {
        schedule.setLink(null);
        mvc.perform(post("/addschedule")
                .flashAttr("schedule", schedule)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(view().name("index"));
    }

    @Test
    void checkIfValidThenShowShowUpdateFormTemplate() throws Exception{
        mvc.perform(get("/edit/" + schedule.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("update-schedule"))
                .andExpect(model().attributeExists("schedule"));
    }

    @Test
    void checkIfShowUpdateFormTemplateButIdNotFoundThenThrowException() {
        try {
            mvc.perform(get("/edit/0"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("update-schedule"))
                    .andExpect(model().attributeExists("schedule"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid schedule id:0",
                    e.getMessage());
        }
    }

    @Test
    void checkIfUpdateScheduleValidThenShowTemplate() throws Exception {
        mvc.perform(post("/update/" + schedule.getId())
                .flashAttr("schedule", schedule)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/"));
    }

    @Test
    void checkIfUpdateScheduleNotValidThenStay() throws Exception {
        schedule.setLink(null);
        mvc.perform(post("/update/" +  schedule.getId())
                .flashAttr("schedule", schedule)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/"));
    }

    @Test
    void checkIfValidThenShowDeleteScheduleTemplate() throws Exception{
        mvc.perform(get("/delete/" + schedule.getId()))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/"));
    }

    @Test
    void checkIfShowDeleteScheduleTemplateButIdNotFoundThenThrowException() {
        try {
            mvc.perform(get("/delete/0"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("index"));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid schedule id:0",
                    e.getMessage());}
    }

    @Test
    void checkIfUpdateScheduleWithWeekendDateThenRedirectToErrorPage() throws Exception {
        schedule.setDate(LocalDate.parse("2020-06-06"));
        mvc.perform(post("/update/" +  schedule.getId())
                .flashAttr("schedule", schedule)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/error-date"));
    }

    @Test
    void checkIfAddScheduleWithWeekendDateThenRedirectToErrorPage() throws Exception {
        schedule.setDate(LocalDate.parse("2020-06-06"));
        mvc.perform(post("/addschedule/")
                .flashAttr("schedule", schedule)
                .flashAttr("result", bindingResult)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("error-date"));
    }
}

package com.classtime.ClassTime.event;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.*;
import java.util.*;
import static org.assertj.core.api.Assertions.*;

public class ScheduleTest {
    private Schedule schedule;

    @BeforeEach
    void setUp() {
        schedule = new Schedule("softeng", "https://www.meet.google.com", LocalDate.of(2020, 05, 29), LocalTime.of(9, 0), LocalTime.of(10, 0));
    }

    @Test
    void testSetId() throws Exception{
        schedule.setId(1);
        assertThat(schedule.getId()).isEqualTo(1);
    }

    @Test
    void testName() throws Exception{
        String name = "softeng";
        schedule.setName(name);
        assertThat(schedule.getName().equals("name"));
    }

    @Test
    void testLink() throws Exception{
        String link = "https://www.google.com";
        schedule.setLink(link);
        assertThat(schedule.getLink().equals("link"));
    }

    @Test
    void testDate() throws Exception{
        LocalDate date = LocalDate.of(2020, 05, 29);
        schedule.setDate(date);
        assertThat(schedule.getDate().equals("date"));
    }

    @Test
    void testStartTime() throws Exception{
        LocalTime start = LocalTime.of(9, 0);
        schedule.setStartTime(start);
        assertThat(schedule.getStartTime().equals("start"));
    }

    @Test
    void testEndTime() throws Exception{
        LocalTime end = LocalTime.of(10, 0);
        schedule.setEndTime(end);
        assertThat(schedule.getEndTime().equals("end"));
    }

    @Test
    void testDayOfWeek() throws Exception{
        LocalDate date = LocalDate.of(2020, 05, 29);
        DayOfWeek dow = date.getDayOfWeek();
        assertThat(schedule.getDayOfWeek().equals("dow"));
    }
}

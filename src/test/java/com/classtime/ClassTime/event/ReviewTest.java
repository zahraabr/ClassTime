package com.classtime.ClassTime.event;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.*;
import java.util.*;
import static org.assertj.core.api.Assertions.*;


public class ReviewTest {
    private Review review;
    private Schedule schedule;

    @BeforeEach
    void setUp() {
        schedule = new Schedule("softeng", "https://www.meet.google.com", LocalDate.of(2020, 05, 29), LocalTime.of(9, 0), LocalTime.of(10, 0));
        review = new Review(schedule, LocalDate.now(), "review", "test");
    }

    @Test
    void testSetId() throws Exception{
        review.setId(1);
        assertThat(review.getId()).isEqualTo(1);
    }

    @Test
    void testSchedule() throws Exception{
        Schedule schedule = new Schedule("softeng", "https://www.meet.google.com", LocalDate.of(2020, 05, 29), LocalTime.of(9, 0), LocalTime.of(10, 0));;
        review.setSchedule(schedule);
        assertThat(review.getSchedule().equals("schedule"));
    }

    @Test
    void testTitle() throws Exception{
        String title = "review";
        review.setTitle(title);
        assertThat(review.getTitle().equals("title"));
    }

    @Test
    void testDate() throws Exception{
        LocalDate date = LocalDate.now();
        review.setDate(date);
        assertThat(review.getDate().equals("date"));
    }

    @Test
    void testContent() throws Exception{
        String content = "test";
        review.setContent(content);
        assertThat(review.getContent().equals("content"));
    }
}
